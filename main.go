package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"todo-api/configuration"
	"todo-api/handler"
	"todo-api/repository"
	"todo-api/services"
)

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	//Config
	config := configuration.CreateMongoClient()
	//Repository
	todoRepository := repository.NewTodoRepository(config)
	//Service
	todoService := services.NewTodoService(todoRepository)
	//Handler
	todoHandler := handler.NewTodoHandler(todoService)

	e.GET("/todos", todoHandler.GetTodoList)
	e.POST("/todos", todoHandler.CreateTodo)
	e.GET("/todos/:id", todoHandler.GetTodo)
	e.PUT("/todos/:id/", todoHandler.UpdateTodo)
	e.DELETE("/todos/:id", todoHandler.DeleteTodo)

	e.Logger.Fatal(e.Start(":8080"))

	log.Debug("Server Started")
}
