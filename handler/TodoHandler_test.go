package handler

import (
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"todo-api/mock"
	"todo-api/model"
)

func TestTodoHandler_CreateTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	request := model.TodoRequest{Content: "TEST", Completed: false}
	requestAsString := `{"content":"TEST", "completed":false}`
	expected := model.Todo{
		ID:        "12345testasfasd145",
		Content:   "TEST",
		Completed: false,
	}
	expectedAsString := `{"id":"12345testasfasd145","content":"TEST","completed":false}`

	service := mock.NewMockService(controller)
	service.EXPECT().Create(gomock.Eq(request)).Return(expected).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/todos", strings.NewReader(requestAsString))
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.CreateTodo(ctx)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, expectedAsString, rec.Body.String())
	}
}

func TestTodoHandler_DeleteTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := mock.NewMockService(controller)
	service.EXPECT().Delete(gomock.Eq("12345testasfasd145")).Return(true)
	handler := TodoHandler{service}

	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/todos/12345", nil)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)
	ctx.SetParamNames("id")
	ctx.SetParamValues("12345testasfasd145")

	if assert.NoError(t, handler.DeleteTodo(ctx)) {
		assert.Equal(t, http.StatusNoContent, rec.Code)
	}
}

func TestTodoHandler_GetTodoList(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todos []model.Todo
	todos = append(todos, model.Todo{
		ID:        "12345testasfasd145",
		Content:   "TEST",
		Completed: false,
	})

	service := mock.NewMockService(controller)
	service.EXPECT().FindAll().Return(todos).Times(1)
	handler := TodoHandler{service}

	expected := `[{"id":"12345testasfasd145","content":"TEST","completed":false}]
`

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.GetTodoList(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}
