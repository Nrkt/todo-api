package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"todo-api/model"
	"todo-api/services"
)

type TodoHandler struct {
	service services.Service
}

func NewTodoHandler(service services.Service) TodoHandler {
	return TodoHandler{service}
}

func (h TodoHandler) CreateTodo(ctx echo.Context) error {
	request := &model.TodoRequest{}

	if err := ctx.Bind(request); err != nil {
		return err
	}

	response := h.service.Create(request)

	if response == nil {
		return ctx.JSON(http.StatusInternalServerError, "There was an error while processing ")
	}

	return ctx.JSON(http.StatusCreated, response)
}

func (h TodoHandler) UpdateTodo(ctx echo.Context) error {
	var request model.TodoRequest
	_ = ctx.Bind(&request)
	id := ctx.Param("id")
	response := h.service.Update(id, request)
	if response == nil {
		return ctx.JSON(http.StatusInternalServerError, "There was an error while processing ")
	}

	return ctx.JSON(http.StatusOK, response)
}

func (h TodoHandler) DeleteTodo(ctx echo.Context) error {
	id := ctx.Param("id")
	if h.service.Delete(id) == false {
		return ctx.JSON(http.StatusInternalServerError, "There was an error while processing ")
	}
	return ctx.NoContent(http.StatusNoContent)
}

func (h TodoHandler) GetTodoList(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, h.service.FindAll())
}

func (h TodoHandler) GetTodo(ctx echo.Context) error {
	id := ctx.Param("id")
	todo := h.service.FindOne(id)
	if todo == nil {
		return ctx.JSON(http.StatusNotFound, "Could not find a todo with the specified value ")
	}

	return ctx.JSON(http.StatusOK, todo)
}
