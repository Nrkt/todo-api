package repository

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"todo-api/model"
)

type TodoRepository struct {
	collection *mongo.Collection
}

func NewTodoRepository(collection *mongo.Collection) Repository {
	return TodoRepository{collection: collection}
}

func (t TodoRepository) FindAll() interface{} {
	var todolist = make([]model.Todo, 0)
	result, err := t.collection.Find(context.Background(), bson.M{})
	if err != nil {
		log.Error(err)
		return nil
	}

	_ = result.All(context.Background(), &todolist)

	return todolist
}

func (t TodoRepository) Find(id string) interface{} {
	todo := &model.Todo{}
	result := t.collection.FindOne(context.Background(), GetObjectId(id))
	err := result.Decode(todo)
	if err != nil {
		log.Error(err)
		return nil
	}

	return todo
}

func (t TodoRepository) Create(i interface{}) interface{} {
	result, err := t.collection.InsertOne(context.Background(), i)

	if err != nil {
		log.Error(err)
		return nil
	}

	objectId := result.InsertedID.(primitive.ObjectID)

	todo := &model.Todo{}

	content := t.collection.FindOne(context.Background(), bson.M{"_id": objectId})
	_ = content.Decode(&todo)

	return todo
}

func (t TodoRepository) Update(i interface{}) interface{} {
	todo := i.(model.Todo)

	value := bson.D{
		{"$set",
			bson.D{
				{"content", todo.Content}, {"completed", todo.Completed}}}}

	_, err := t.collection.UpdateOne(context.Background(), GetObjectId(todo.ID), value)

	if err != nil {
		log.Error(err)
		return nil
	}

	content := t.collection.FindOne(context.Background(), GetObjectId(todo.ID))
	_ = content.Decode(&todo)
	return todo
}

func (t TodoRepository) Delete(id string) bool {
	_, err := t.collection.DeleteOne(context.Background(), GetObjectId(id))
	if err != nil {
		log.Error(err)
		return false
	}
	return true
}

func GetObjectId(id string) bson.M {
	objectId, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objectId}
	return filter
}
