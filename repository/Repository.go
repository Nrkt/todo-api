package repository

type Repository interface {
	FindAll() interface{}
	Find(id string) interface{}
	Create(i interface{}) interface{}
	Update(i interface{}) interface{}
	Delete(id string) bool
}

