package configuration

import (
	"context"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

func CreateMongoClient() *mongo.Collection {
	clientOptions := options.Client().ApplyURI(DBUri())
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Error(err)
	}

	if err := client.Ping(context.TODO(), nil);

	err != nil {
		log.Fatal(err)
	}

	log.Info("Connected to MongoDB!")

	return client.Database(DBName()).Collection(CollectionName())
}

func DBUri() string {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		return "mongodb://admin:root@localhost:27017/?authSource=admin&readPreference=primary&ssl=false"
	}
	return uri
}

func DBName() string {
	dbname := os.Getenv("MONGODB_DATABASE_NAME")
	if dbname == "" {
		return "tasks"
	}

	return dbname
}

func CollectionName() string {
	collectionName := os.Getenv("MONGODB_COLLECTION_NAME")
	if collectionName == "" {
		return "todos"
	}

	return collectionName
}