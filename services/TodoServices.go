package services

import (
	"todo-api/model"
	"todo-api/repository"
)

type TodoService struct {
	repository repository.Repository
}

func NewTodoService(repository repository.Repository) Service {
	return TodoService{repository}
}

func (t TodoService) FindAll() interface{} {
	return t.repository.FindAll()
}

func (t TodoService) FindOne(id string) interface{} {
	return t.repository.Find(id)
}

func (t TodoService) Create(i interface{}) interface{} {
	return t.repository.Create(i)
}

func (t TodoService) Update(id string, i interface{}) interface{} {
	requestTodo := i.(model.TodoRequest)
	exitTodo := t.repository.Find(id)
	if exitTodo == nil {
		return nil
	}

	todo := exitTodo.(model.Todo)
	todo.Content = requestTodo.Content
	todo.Completed = requestTodo.Completed
	return t.repository.Update(todo)
}

func (t TodoService) Delete(id string) bool {
	return t.repository.Delete(id)
}
