package services

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
	"todo-api/mock"
	"todo-api/model"
)


func TestTodoService_Create(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := mock.NewMockRepository(controller)
	repository.EXPECT().Create(model.TodoRequest{Content: "TestContent", Completed: false}).Return(model.Todo{
		ID:        "123Test45",
		Content:   "TestContent",
		Completed: false,
	}).Times(1)

	service := NewTodoService(repository)
	created := service.Create(model.TodoRequest{Content: "TestContent", Completed: false})

	assert.NotNil(t, created)
	todo := created.(model.Todo)
	assert.Equal(t, "123Test45", todo.ID)
	assert.Equal(t, "TestTitle", todo.Content)
	assert.False(t, todo.Completed)

}

func TestTodoService_Delete(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := mock.NewMockRepository(controller)
	repository.EXPECT().Delete(gomock.Eq("123Test45")).Times(1)

	service := TodoService{repository}
	service.Delete("123Test45")
}

func TestTodoService_FindAll(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todoList []model.Todo
	todoList = append(todoList, model.Todo{
		ID:        "123Test45",
		Content:   "TestContent",
		Completed: false,
	})

	repository := mock.NewMockRepository(controller)
	repository.EXPECT().FindAll().Return(todoList).Times(1)
	service := TodoService{repository}

	all := service.FindAll().([]model.Todo)
	assert.NotNil(t, all)
	assert.Equal(t, 1, len(all))
	assert.Equal(t, "123Test45", all[0].ID)
	assert.Equal(t, "TestContent", all[0].Content)
	assert.False(t, all[0].Completed)

}
