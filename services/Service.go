package services

type Service interface {
	FindAll() interface{}
	FindOne(id string) interface{}
	Create(i interface{}) interface{}
	Update(id string, i interface{}) interface{}
	Delete(id string) bool
}
