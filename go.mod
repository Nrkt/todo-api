module todo-api

go 1.16

require (
	github.com/golang/mock v1.5.0
	github.com/labstack/echo/v4 v4.2.2
	github.com/labstack/gommon v0.3.0
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.5.1
)
