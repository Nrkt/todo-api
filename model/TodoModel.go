package model

type Todo struct {
	ID        string `json:"id" bson:"_id,omitempty"`
	Content   string `json:"content"  bson:"content"`
	Completed bool   `json:"completed" bson:"completed"`
}

type TodoRequest struct {
	Content   string `json:"content" `
	Completed bool   `json:"completed"`
}
